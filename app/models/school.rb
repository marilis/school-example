# frozen_string_literal: true

class School < ApplicationRecord
  validates :name, presence: true, length: { minimum: 1, maximum: 128 }
  validates :address, presence: true, length: { minimum: 2, maximum: 128 }
  has_many :teachers, dependent: :nullify
end

# == Schema Information
#
# Table name: schools
#
#  id         :bigint(8)        not null, primary key
#  address    :string
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
