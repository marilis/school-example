# frozen_string_literal: true

class Team < ApplicationRecord
  validates :name, length: { minimum: 3, maximum: 128 }
  belongs_to :teamlead, optional: true
  has_many :students, dependent: :nullify
end

# == Schema Information
#
# Table name: teams
#
#  id          :bigint(8)        not null, primary key
#  name        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  teamlead_id :bigint(8)
#
# Indexes
#
#  index_teams_on_teamlead_id  (teamlead_id)
#
