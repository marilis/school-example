# frozen_string_literal: true

class Group < ApplicationRecord
  validates :name, length: { minimum: 3, maximum: 128 }, uniqueness: true
  has_many :students, dependent: :nullify
  belongs_to :teacher, optional: true
end

# == Schema Information
#
# Table name: groups
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  teacher_id :bigint(8)
#
# Indexes
#
#  index_groups_on_teacher_id  (teacher_id)
#
