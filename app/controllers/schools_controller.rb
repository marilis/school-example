# frozen_string_literal: true

class SchoolsController < ApplicationController
  before_action :load_school, only: %i[show edit update destroy]

  def index
    @q = School.ransack(search_params)
    @schools = @q.result.order(:name).page(params[:page])
    @teachers = Teacher.all
  end

  def new
    @school = School.new
  end

  def create
    @school = School.new(school_params)

    if @school.save
      redirect_to @school, notice: 'School was successfully created.'
    else
      render :new
    end
  end

  def update
    if @school.update(school_params)
      redirect_to @school, notice: 'School was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @school.destroy
    redirect_to schools_url, notice: 'School was successfully destroyed.'
  end

  private

  def load_school
    @school = School.find(params[:id])
  end

  def school_params
    params.require(:school).permit(:name, :address)
  end

  def search_params
    params[:q]&.permit(%w[name_cont address_cont teachers_id_eq])
  end
end
