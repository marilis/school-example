# frozen_string_literal: true

class TeamsController < ApplicationController
  before_action :load_team, only: %i[show edit update destroy]

  def index
    @q = Team.ransack(search_params)
    @teams = @q.result.order(:name).page(params[:page])
    @teamleads = Teamlead.all
  end

  def new
    @team = Team.new
  end

  def create
    @team = Team.new(team_params)

    if @team.save
      redirect_to @team, notice: 'Team was successfully created.'
    else
      render :new
    end
  end

  def update
    if @team.update(team_params)
      redirect_to @team, notice: 'Team was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @team.destroy
    redirect_to teams_url, notice: 'Team was successfully deleted.'
  end

  private

  def load_team
    @team = Team.find(params[:id])
  end

  def team_params
    params.require(:team).permit(:name, :teamlead_id)
  end

  def search_params
    params[:q]&.permit(%w[name_cont teamlead_id_eq])
  end
end
