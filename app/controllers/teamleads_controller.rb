# frozen_string_literal: true

class TeamleadsController < ApplicationController
  before_action :load_teamlead, only: %i[show edit update destroy]

  def index
    @q = Teamlead.ransack(search_params)
    @teamleads = @q.result.order(:name).page(params[:page])
    @teams = Team.all
  end

  def new
    @teamlead = Teamlead.new
  end

  def create
    @teamlead = Teamlead.new(teamlead_params)
    if @teamlead.save
      redirect_to @teamlead, notice: 'Teamlead was successfully created.'
    else
      render :new
    end
  end

  def update
    if @teamlead.update(teamlead_params)
      redirect_to @teamlead, notice: 'Teamlead was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @teamlead.destroy
    redirect_to teamleads_path, notice: 'Teamlead was successfully deleted.'
  end

  private

  def load_teamlead
    @teamlead = Teamlead.find(params[:id])
  end

  def teamlead_params
    params.require(:teamlead).permit(:name, :email, :msisdn, :password)
  end

  def search_params
    params[:q]&.permit(%w[name_cont email_cont msisdn_cont])
  end
end
