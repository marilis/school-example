# frozen_string_literal: true

class GroupsController < ApplicationController
  before_action :load_group, only: %i[show edit update destroy]

  def index
    @q = Group.ransack(search_params)
    @groups = @q.result.order(:name).page(params[:page])
    @teachers = Teacher.all
  end

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(group_params)

    if @group.save
      redirect_to @group, notice: 'Group was successfully created.'
    else
      render :new
    end
  end

  def update
    if @group.update(group_params)
      redirect_to @group, notice: 'Group was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @group.destroy
    redirect_to groups_url, notice: 'Group was successfully destroyed.'
  end

  private

  def load_group
    @group = Group.find(params[:id])
  end

  def group_params
    params.require(:group).permit(:name, :teacher_id)
  end

  def search_params
    params[:q]&.permit(%w[name_cont teacher_id_eq])
  end
end
