# frozen_string_literal: true

class TeachersController < ApplicationController
  before_action :load_teacher, only: %i[show edit update destroy]

  def index
    @q = Teacher.ransack(search_params)
    @teachers = @q.result.order(:name).page(params[:page])
    @schools = School.all
  end

  def new
    @teacher = Teacher.new
  end

  def create
    @teacher = Teacher.new(teacher_params)
    if @teacher.save
      redirect_to @teacher, notice: 'Teacher was successfully created.'
    else
      render :new
    end
  end

  def update
    if @teacher.update(teacher_params)
      redirect_to @teacher, notice: 'Teacher was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @teacher.destroy
    redirect_to teachers_path, notice: 'Teacher was successfully deleted.'
  end

  private

  def load_teacher
    @teacher = Teacher.find(params[:id])
  end

  def teacher_params
    params.require(:teacher).permit(:name, :email, :msisdn, :school_id, :password)
  end

  def search_params
    params[:q]&.permit(%w[name_cont email_cont msisdn_cont school_id_eq])
  end
end
