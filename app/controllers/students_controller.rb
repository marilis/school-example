# frozen_string_literal: true

class StudentsController < ApplicationController
  before_action :load_student, only: %i[show edit update destroy]

  def index
    @q = Student.ransack(search_params)
    @students = @q.result.order(:name).page(params[:page])
    @groups = Group.all
    @teams = Team.all
  end

  def new
    @student = Student.new
  end

  def create
    @student = Student.new(student_params)
    if @student.save
      redirect_to @student, notice: 'Student was successfully created.'
    else
      render :new
    end
  end

  def update
    if @student.update(student_params)
      redirect_to @student, notice: 'Student was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @student.destroy
    redirect_to students_path, notice: 'Student was successfully deleted.'
  end

  private

  def load_student
    @student = Student.find(params[:id])
  end

  def student_params
    params.require(:student).permit(:name, :email, :msisdn, :team_id, :group_id, :password)
  end

  def search_params
    params[:q]&.permit(%w[name_cont email_cont msisdn_cont group_id_eq team_id_eq])
  end
end
