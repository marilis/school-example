module ApplicationHelper
  def get_icon(user)
    case user.type
    when 'Student'
      'user-graduate'
    when 'Teamlead'
      'sitemap'
    when 'Teacher'
      'chalkboard-teacher'
    else
      ''
    end
  end
end
