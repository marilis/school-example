require 'rails_helper'

targets = %w[student teamlead teacher team group school]

RSpec.describe "Unauthorized access attempt to #{targets.join(', ')}", type: :request do
  it 'redirects to login form' do
    routes = Rails.application.routes.routes.map { |r| { path: r.path.spec.to_s.sub(/\(.+\)\z/, '').sub(':id', '1'), method: r.verb } }
                  .select { |r| targets.any? { |target| r[:path].include?(target) } }

    routes.each do |route|
      case route[:method]
      when 'GET'
        expect(get(route[:path])).to redirect_to(new_user_session_path)
      when 'POST'
        expect(post(route[:path])).to redirect_to(new_user_session_path)
      when 'PATCH'
        expect(patch(route[:path])).to redirect_to(new_user_session_path)
      when 'DELETE'
        expect(delete(route[:path])).to redirect_to(new_user_session_path)
      end
    end
  end
end
