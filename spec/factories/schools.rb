FactoryBot.define do
  factory :school, class: School do
    name { Faker::Educator.university }
    address { Faker::Address.full_address }

    after :create do |school|
      create_list :teacher, 5, school: school
    end
  end

  factory :school_invalid, class: School do
    name { 'd' }
    address { '' }
  end
end

# == Schema Information
#
# Table name: schools
#
#  id         :bigint(8)        not null, primary key
#  address    :string
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
