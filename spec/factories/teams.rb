FactoryBot.define do
  factory :team, class: Team do
    name { "#{Faker::ProgrammingLanguage.unique.name} Team" }
    teamlead
  end

  factory :team_invalid, class: Team do
    name { '' }
    teamlead
  end
end

# == Schema Information
#
# Table name: teams
#
#  id          :bigint(8)        not null, primary key
#  name        :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  teamlead_id :bigint(8)
#
# Indexes
#
#  index_teams_on_teamlead_id  (teamlead_id)
#
