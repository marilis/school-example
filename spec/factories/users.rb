FactoryBot.define do
  factory :user, class: User do
    name { Faker::Name.unique.name }
    email { "#{name.downcase.split.join('.').gsub(/\.+/, '.').gsub(/\.\z/, '')}@example.com" }
    msisdn { Faker::PhoneNumber.unique.phone_number_with_country_code.gsub(/[\.\-\s\(\)]/, '')[0..11] }
    password { 'password' }
  end

  factory :user_invalid, class: User do
    name { '' }
    email { 'отстань' }
    msisdn { 'nokla' }
    password { '111' }
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :bigint(8)        not null, primary key
#  email                  :string           not null
#  encrypted_password     :string           default(""), not null
#  msisdn                 :string
#  name                   :string           not null
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  type                   :string           not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  group_id               :bigint(8)
#  school_id              :bigint(8)
#  team_id                :bigint(8)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_group_id              (group_id)
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_school_id             (school_id)
#  index_users_on_team_id               (team_id)
#
