FactoryBot.define do
  factory :group, class: Group do
    name { "Group - #{Faker::Team.unique.name}" }

    after :create do |group|
      _students = create_list :student, 15, group: group
    end
  end

  factory :group_invalid, class: Group do
    name { '' }

    after :create do |group|
      _students = create_list :student, 15, group: group
    end
  end
end

# == Schema Information
#
# Table name: groups
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  teacher_id :bigint(8)
#
# Indexes
#
#  index_groups_on_teacher_id  (teacher_id)
#
