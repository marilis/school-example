require 'rails_helper'

describe TeachersController do
  
  before { sign_in create(:teacher) }

  describe 'GET #index' do
    it 'render :index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'returns successful response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns successful response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders :new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns new Teacher to @teacher' do
      get :new
      expect(assigns(:teacher)).to be_a_new(Teacher)
    end
  end

  describe 'GET #show' do
    let(:teacher) { create(:teacher) }

    it 'returns successful response' do
      get :show, params: { id: teacher }
      expect(response).to be_successful
    end

    it 'renders :show template' do
      get :show, params: { id: teacher }
      expect(response).to render_template(:show)
    end

    it 'assigns requested Teacher to @teacher' do
      get :show, params: { id: teacher }
      expect(assigns(:teacher)).to eq(teacher)
    end
  end

  describe 'POST #create' do
    context 'valid data' do
      it 'redirects to show action' do
        post :create, params: { teacher: attributes_for(:teacher) }
        expect(response).to redirect_to(teacher_path(assigns(:teacher)))
      end

      it 'creates a new Teacher' do
        expect do
          post :create, params: { teacher: attributes_for(:teacher) }
        end.to change(Teacher, :count).by(1)
      end
    end
  end

  context 'invalid data' do
    it 'renders :new template' do
      post :create, params: { teacher: attributes_for(:teacher_invalid) }
      expect(response).to render_template(:new)
    end

    it 'it doesn\'t create new teacher in database' do
      expect do
        post :create, params: { teacher: attributes_for(:teacher_invalid) }
      end.not_to change(Teacher, :count)
    end
  end

  describe 'GET #edit' do
    let(:teacher) { create(:teacher) }

    it 'renders :edit template' do
      get :edit, params: { id: teacher }
      expect(response).to render_template(:edit)
    end

    it 'assigns the requested instance into the template' do
      get :edit, params: { id: teacher }
      expect(assigns(:teacher)).to eq(teacher)
    end
  end

  describe 'PUT #update' do
    let(:teacher) { create(:teacher) }

    context 'valid data' do
      let(:new_name) { 'Gordon Shumway' }
      let(:valid_data) { attributes_for(:teacher, name: new_name) }
      it 'redirects to teachers#show' do
        put :update, params: { id: teacher, teacher: valid_data }
        expect(response).to redirect_to(teacher)
      end

      it 'updates teachers in the database' do
        put :update, params: { id: teacher, teacher: valid_data }
        teacher.reload
        expect(teacher.name).to eq(new_name)
      end
    end

    context 'invalid data' do
      let(:new_invalid_name) { 'G' }
      let(:new_invalid_msisdn) { '+12323' }
      let(:invalid_data) { attributes_for(:teacher, new_invalid_name: new_invalid_name, msisdn: new_invalid_msisdn) }
      it 'it renders :edit template' do
        put :update, params: { id: teacher, teacher: invalid_data }
        expect(response).to render_template(:edit)
      end

      it 'doesn\'t update data in the database' do
        put :update, params: { id: teacher, teacher: invalid_data }
        teacher.reload
        expect(teacher.name).not_to eq(new_invalid_name)
        expect(teacher.msisdn).not_to eq(new_invalid_msisdn)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:teacher) { create(:teacher) }

    it 'redirects to teachers#index' do
      delete :destroy, params: { id: teacher }
      expect(response).to redirect_to(teachers_path)
    end

    it 'deletes teacher from database' do
      delete :destroy, params: { id: teacher }
    end
  end
end
