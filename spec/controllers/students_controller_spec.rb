require 'rails_helper'

describe StudentsController do
  before { sign_in create(:teacher) }

  describe 'GET #index' do
    it 'render :index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'returns successful response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns successful response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders :new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns new Student to @student' do
      get :new
      expect(assigns(:student)).to be_a_new(Student)
    end
  end

  describe 'GET #show' do
    let(:student) { create(:student) }

    it 'returns successful response' do
      get :show, params: { id: student }
      expect(response).to be_successful
    end

    it 'renders :show template' do
      get :show, params: { id: student }
      expect(response).to render_template(:show)
    end

    it 'assigns requested Student to @student' do
      get :show, params: { id: student }
      expect(assigns(:student)).to eq(student)
    end
  end

  describe 'POST #create' do
    context 'valid data' do
      it 'creates a new Student' do
        expect do
          post :create, params: { student: attributes_for(:student) }
        end.to change(Student, :count).by(1)
      end

      it 'redirects to show action' do
        post :create, params: { student: attributes_for(:student) }
        expect(response).to redirect_to(student_path(assigns(:student)))
      end
    end
  end

  context 'invalid data' do
    it 'renders :new template' do
      post :create, params: { student: attributes_for(:student_invalid) }
      expect(response).to render_template(:new)
    end

    it 'it doesn\'t create new student in database' do
      expect do
        post :create, params: { student: attributes_for(:student_invalid) }
      end.not_to change(Student, :count)
    end
  end

  describe 'GET #edit' do
    let(:student) { create(:student) }

    it 'renders :edit template' do
      get :edit, params: { id: student }
      expect(response).to render_template(:edit)
    end

    it 'assigns the requested instance into the template' do
      get :edit, params: { id: student }
      expect(assigns(:student)).to eq(student)
    end
  end

  describe 'PUT #update' do
    let(:student) { create(:student) }

    context 'valid data' do
      let(:new_name) { 'Gordon Shumway' }
      let(:valid_data) { attributes_for(:student, name: new_name) }
      it 'redirects to students#show' do
        put :update, params: { id: student, student: valid_data }
        expect(response).to redirect_to(student)
      end

      it 'updates students in the database' do
        put :update, params: { id: student, student: valid_data }
        student.reload
        expect(student.name).to eq(new_name)
      end
    end

    context 'invalid data' do
      let(:new_invalid_name) { 'G' }
      let(:new_invalid_msisdn) { '+12323' }
      let(:invalid_data) { attributes_for(:student, new_invalid_name: new_invalid_name, msisdn: new_invalid_msisdn) }
      it 'it renders :edit template' do
        put :update, params: { id: student, student: invalid_data }
        expect(response).to render_template(:edit)
      end

      it 'doesn\'t update data in the database' do
        put :update, params: { id: student, student: invalid_data }
        student.reload
        expect(student.name).not_to eq(new_invalid_name)
        expect(student.msisdn).not_to eq(new_invalid_msisdn)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:student) { create(:student) }

    it 'redirects to students#index' do
      delete :destroy, params: { id: student }
      expect(response).to redirect_to(students_path)
    end

    it 'deletes student from database' do
      delete :destroy, params: { id: student }
    end
  end
end
