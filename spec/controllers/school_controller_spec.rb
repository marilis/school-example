require 'rails_helper'

describe SchoolsController do
  
  before { sign_in create(:teacher) }

  describe 'GET #index' do
    it 'render :index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'returns successful response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns successful response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders :new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns new School to @school' do
      get :new
      expect(assigns(:school)).to be_a_new(School)
    end
  end

  describe 'GET #show' do
    let(:school) { create(:school) }

    it 'returns successful response' do
      get :show, params: { id: school }
      expect(response).to be_successful
    end

    it 'renders :show template' do
      get :show, params: { id: school }
      expect(response).to render_template(:show)
    end

    it 'assigns requested School to @school' do
      get :show, params: { id: school }
      expect(assigns(:school)).to eq(school)
    end
  end

  describe 'POST #create' do
    context 'valid data' do
      it 'redirects to show action' do
        post :create, params: { school: attributes_for(:school) }
        expect(response).to redirect_to(school_path(assigns(:school)))
      end

      it 'creates a new School' do
        expect do
          post :create, params: { school: attributes_for(:school) }
        end.to change(School, :count).by(1)
      end
    end
  end

  context 'invalid data' do
    it 'renders :new template' do
      post :create, params: { school: attributes_for(:school_invalid) }
      expect(response).to render_template(:new)
    end

    it 'it doesn\'t create new school in database' do
      expect do
        post :create, params: { school: attributes_for(:school_invalid) }
      end.not_to change(School, :count)
    end
  end

  describe 'GET #edit' do
    let(:school) { create(:school) }

    it 'renders :edit template' do
      get :edit, params: { id: school }
      expect(response).to render_template(:edit)
    end

    it 'assigns the requested instance into the template' do
      get :edit, params: { id: school }
      expect(assigns(:school)).to eq(school)
    end
  end

  describe 'PUT #update' do
    let(:school) { create(:school) }

    context 'valid data' do
      let(:new_name) { 'New School Name' }
      let(:valid_data) { attributes_for(:school, name: new_name) }

      it 'redirects to schools#show' do
        put :update, params: { id: school, school: valid_data }
        expect(response).to redirect_to(school)
      end

      it 'updates schools in the database' do
        put :update, params: { id: school, school: valid_data }
        school.reload
        expect(school.name).to eq(new_name)
      end
    end

    context 'invalid data' do
      let(:new_invalid_name) { '' }
      let(:invalid_data) { attributes_for(:school, name: new_invalid_name) }

      it 'it renders :edit template' do
        put :update, params: { id: school, school: invalid_data }
        expect(response).to render_template(:edit)
      end

      it 'doesn\'t update data in the database' do
        put :update, params: { id: school, school: invalid_data }
        school.reload
        expect(school.name).not_to eq(new_invalid_name)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:school) { create(:school) }

    it 'redirects to schools#index' do
      delete :destroy, params: { id: school }
      expect(response).to redirect_to(schools_path)
    end

    it 'deletes school from database' do
      delete :destroy, params: { id: school }
    end
  end
end
