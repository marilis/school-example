require 'rails_helper'

describe GroupsController do
  
  before { sign_in create(:teacher) }

  describe 'GET #index' do
    it 'render :index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'returns successful response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns successful response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders :new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns new Group to @group' do
      get :new
      expect(assigns(:group)).to be_a_new(Group)
    end
  end

  describe 'GET #show' do
    let(:group) { create(:group) }

    it 'returns successful response' do
      get :show, params: { id: group }
      expect(response).to be_successful
    end

    it 'renders :show template' do
      get :show, params: { id: group }
      expect(response).to render_template(:show)
    end

    it 'assigns requested Group to @group' do
      get :show, params: { id: group }
      expect(assigns(:group)).to eq(group)
    end
  end

  describe 'POST #create' do
    context 'valid data' do
      it 'redirects to show action' do
        post :create, params: { group: attributes_for(:group) }
        expect(response).to redirect_to(group_path(assigns(:group)))
      end

      it 'creates a new Group' do
        expect do
          post :create, params: { group: attributes_for(:group) }
        end.to change(Group, :count).by(1)
      end
    end
  end

  context 'invalid data' do
    it 'renders :new template' do
      post :create, params: { group: attributes_for(:group_invalid) }
      expect(response).to render_template(:new)
    end

    it 'it doesn\'t create new group in database' do
      expect do
        post :create, params: { group: attributes_for(:group_invalid) }
      end.not_to change(Group, :count)
    end
  end

  describe 'GET #edit' do
    let(:group) { create(:group) }

    it 'renders :edit template' do
      get :edit, params: { id: group }
      expect(response).to render_template(:edit)
    end

    it 'assigns the requested instance into the template' do
      get :edit, params: { id: group }
      expect(assigns(:group)).to eq(group)
    end
  end

  describe 'PUT #update' do
    let(:group) { create(:group) }

    context 'valid data' do
      let(:new_name) { 'New Group Name' }
      let(:valid_data) { attributes_for(:group, name: new_name) }

      it 'redirects to groups#show' do
        put :update, params: { id: group, group: valid_data }
        expect(response).to redirect_to(group)
      end

      it 'updates groups in the database' do
        put :update, params: { id: group, group: valid_data }
        group.reload
        expect(group.name).to eq(new_name)
      end
    end

    context 'invalid data' do
      let(:new_invalid_name) { '' }
      let(:invalid_data) { attributes_for(:group, name: new_invalid_name) }

      it 'it renders :edit template' do
        put :update, params: { id: group, group: invalid_data }
        expect(response).to render_template(:edit)
      end

      it 'doesn\'t update data in the database' do
        put :update, params: { id: group, group: invalid_data }
        group.reload
        expect(group.name).not_to eq(new_invalid_name)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:group) { create(:group) }

    it 'redirects to groups#index' do
      delete :destroy, params: { id: group }
      expect(response).to redirect_to(groups_path)
    end

    it 'deletes group from database' do
      delete :destroy, params: { id: group }
    end
  end
end
