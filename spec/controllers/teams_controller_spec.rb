require 'rails_helper'

describe TeamsController do
  
  before { sign_in create(:teacher) }

  describe 'GET #index' do
    it 'render :index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'returns successful response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns successful response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders :new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns new Team to @team' do
      get :new
      expect(assigns(:team)).to be_a_new(Team)
    end
  end

  describe 'GET #show' do
    let(:team) { create(:team) }

    it 'returns successful response' do
      get :show, params: { id: team }
      expect(response).to be_successful
    end

    it 'renders :show template' do
      get :show, params: { id: team }
      expect(response).to render_template(:show)
    end

    it 'assigns requested Team to @team' do
      get :show, params: { id: team }
      expect(assigns(:team)).to eq(team)
    end
  end

  describe 'POST #create' do
    context 'valid data' do
      it 'redirects to show action' do
        post :create, params: { team: attributes_for(:team) }
        expect(response).to redirect_to(team_path(assigns(:team)))
      end

      it 'creates a new Team' do
        expect do
          post :create, params: { team: attributes_for(:team) }
        end.to change(Team, :count).by(1)
      end
    end
  end

  context 'invalid data' do
    it 'renders :new template' do
      post :create, params: { team: attributes_for(:team_invalid) }
      expect(response).to render_template(:new)
    end

    it 'it doesn\'t create new team in database' do
      expect do
        post :create, params: { team: attributes_for(:team_invalid) }
      end.not_to change(Team, :count)
    end
  end

  describe 'GET #edit' do
    let(:team) { create(:team) }

    it 'renders :edit template' do
      get :edit, params: { id: team }
      expect(response).to render_template(:edit)
    end

    it 'assigns the requested instance into the template' do
      get :edit, params: { id: team }
      expect(assigns(:team)).to eq(team)
    end
  end

  describe 'PUT #update' do
    let(:team) { create(:team) }

    context 'valid data' do
      let(:new_name) { 'New Group Name' }
      let(:valid_data) { attributes_for(:team, name: new_name) }

      it 'redirects to teams#show' do
        put :update, params: { id: team, team: valid_data }
        expect(response).to redirect_to(team)
      end

      it 'updates teams in the database' do
        put :update, params: { id: team, team: valid_data }
        team.reload
        expect(team.name).to eq(new_name)
      end
    end

    context 'invalid data' do
      let(:new_invalid_name) { '' }
      let(:invalid_data) { attributes_for(:team, name: new_invalid_name) }

      it 'it renders :edit template' do
        put :update, params: { id: team, team: invalid_data }
        expect(response).to render_template(:edit)
      end

      it 'doesn\'t update data in the database' do
        put :update, params: { id: team, team: invalid_data }
        team.reload
        expect(team.name).not_to eq(new_invalid_name)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:team) { create(:team) }

    it 'redirects to teams#index' do
      delete :destroy, params: { id: team }
      expect(response).to redirect_to(teams_path)
    end

    it 'deletes team from database' do
      delete :destroy, params: { id: team }
    end
  end
end
