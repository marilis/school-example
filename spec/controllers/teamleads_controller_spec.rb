require 'rails_helper'

describe TeamleadsController do
  before { sign_in create(:teacher) }

  describe 'GET #index' do
    it 'render :index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'returns successful response' do
      get :index
      expect(response).to be_successful
    end
  end

  describe 'GET #new' do
    it 'returns successful response' do
      get :new
      expect(response).to be_successful
    end

    it 'renders :new template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'assigns new Teamlead to @teamlead' do
      get :new
      expect(assigns(:teamlead)).to be_a_new(Teamlead)
    end
  end

  describe 'GET #show' do
    let(:teamlead) { create(:teamlead) }

    it 'returns successful response' do
      get :show, params: { id: teamlead }
      expect(response).to be_successful
    end

    it 'renders :show template' do
      get :show, params: { id: teamlead }
      expect(response).to render_template(:show)
    end

    it 'assigns requested Teamlead to @teamlead' do
      get :show, params: { id: teamlead }
      expect(assigns(:teamlead)).to eq(teamlead)
    end
  end

  describe 'POST #create' do
    context 'valid data' do
      it 'redirects to show action' do
        post :create, params: { teamlead: attributes_for(:teamlead) }
        expect(response).to redirect_to(teamlead_path(assigns(:teamlead)))
      end

      it 'creates a new Teamlead' do
        expect do
          post :create, params: { teamlead: attributes_for(:teamlead) }
        end.to change(Teamlead, :count).by(1)
      end
    end
  end

  context 'invalid data' do
    it 'renders :new template' do
      post :create, params: { teamlead: attributes_for(:teamlead_invalid) }
      expect(response).to render_template(:new)
    end

    it 'it doesn\'t create new teamlead in database' do
      expect do
        post :create, params: { teamlead: attributes_for(:teamlead_invalid) }
      end.not_to change(Teamlead, :count)
    end
  end

  describe 'GET #edit' do
    let(:teamlead) { create(:teamlead) }

    it 'renders :edit template' do
      get :edit, params: { id: teamlead }
      expect(response).to render_template(:edit)
    end

    it 'assigns the requested instance into the template' do
      get :edit, params: { id: teamlead }
      expect(assigns(:teamlead)).to eq(teamlead)
    end
  end

  describe 'PUT #update' do
    let(:teamlead) { create(:teamlead) }

    context 'valid data' do
      let(:new_name) { 'Gordon Shumway' }
      let(:valid_data) { attributes_for(:teamlead, name: new_name) }
      it 'redirects to teamleads#show' do
        put :update, params: { id: teamlead, teamlead: valid_data }
        expect(response).to redirect_to(teamlead)
      end

      it 'updates teamleads in the database' do
        put :update, params: { id: teamlead, teamlead: valid_data }
        teamlead.reload
        expect(teamlead.name).to eq(new_name)
      end
    end

    context 'invalid data' do
      let(:new_invalid_name) { 'G' }
      let(:new_invalid_msisdn) { '+12323' }
      let(:invalid_data) { attributes_for(:teamlead, new_invalid_name: new_invalid_name, msisdn: new_invalid_msisdn) }
      it 'it renders :edit template' do
        put :update, params: { id: teamlead, teamlead: invalid_data }
        expect(response).to render_template(:edit)
      end

      it 'doesn\'t update data in the database' do
        put :update, params: { id: teamlead, teamlead: invalid_data }
        teamlead.reload
        expect(teamlead.name).not_to eq(new_invalid_name)
        expect(teamlead.msisdn).not_to eq(new_invalid_msisdn)
      end
    end
  end

  describe 'DELETE destroy' do
    let(:teamlead) { create(:teamlead) }

    it 'redirects to teamleads#index' do
      delete :destroy, params: { id: teamlead }
      expect(response).to redirect_to(teamleads_path)
    end

    it 'deletes teamlead from database' do
      delete :destroy, params: { id: teamlead }
    end
  end
end
