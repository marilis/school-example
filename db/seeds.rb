# frozen_string_literal: true

FactoryBot.create_list(:team, 4)
FactoryBot.create_list(:school, 2)
