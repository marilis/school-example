class AddTeamleadToToTeam < ActiveRecord::Migration[5.2]
  def change
    add_reference :teams, :teamlead, index: true, foreign_key: true
  end
end
