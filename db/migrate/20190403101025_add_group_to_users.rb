class AddGroupToUsers < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :group, index: true, foreign_key: true
  end
end
