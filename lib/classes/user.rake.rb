require 'yaml'
connection_configs = connection_configs = YAML.safe_load(File.read('config/database.yml'))
current_env = ENV['MY_ENV'] || 'development' # if no value, assume development mode
ActiveRecord::Base.establish_connection(connection_configs[current_env])

# class RakeUser
#   def self.reset(params)
#     if params[:email]
#       u = User.find_by(email: params[:email])

#     else
#       puts 'Email must be provided'
#     end
#   end
# end

# RakeUser.reset(email: "luigi.o'keefe@example.com")
puts User.count