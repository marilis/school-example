namespace :user do
  desc 'Destroys user found by email. Expected arguments: [email]'
  task :remove, %i[email] => [:environment] do |_, args|
    user = User.find_by(email: args[:email])
    if user
      user.destroy
      puts 'User destroyed'
    else
      puts 'No such user'
    end
  end
end
