namespace :user do
  desc 'Set login and/or password for a user found by email.'
  task :reset, %i[email new_email new_password] => [:environment] do |_, args|
    email = args[:email]
    new_email = args[:new_email]
    new_password = args[:new_password]

    if new_email.nil? && new_password.nil?
      puts 'At least one argument must be provided: email or password'
      puts 'Skipping task...'
      next
    end

    if email.nil? || email.empty?
      puts 'Search email can`t be empty.'
    else
      user = User.find_by(email: email)
      if user
        user.email = new_email if new_email
        user.password = new_password if new_password
        if user.save
          puts 'Successfully reset user'
        else
          puts user.errors.full_messages unless user.save
        end
      else
        puts 'User not found.'
      end
    end
  end
end
