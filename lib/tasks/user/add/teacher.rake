namespace :user do
  namespace :add do
    desc 'Creates new teacher. Expected arguments: [email, name, msisdn, password]'
    task :teacher, %i[email name msisdn password] => [:environment] do |_, args|
      create_user(Teacher.new, args)
    end

    def create_user(user, args)
      user.email = args[:email]
      user.name = args[:name]
      user.msisdn = args[:msisdn]
      user.password = args[:password]
      if user.save
        puts "User created with id: #{user.id}".green
      else
        puts user.errors.full_messages.red unless user.save
      end
    end
  end
end
