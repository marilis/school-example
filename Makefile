build:
	cp config/database.ci.yml config/database.yml
	bundle install --deployment --clean
test:
	bundle exec rails db:drop db:create db:migrate
	bundle exec rspec
	bundle exec rake db:drop