# Все доступные действия можно узнать командой:
# explico list-actions

milestone :before_deploy do |plan|
  plan.use('deploy/default/before_deploy')

  plan.use('deploy/shared_folders')
  plan.use('deploy/nginx_configtest')

  plan.use('deploy/init', action: 'dry_run')
  plan.use('deploy/init', action: 'stop')
end

milestone :install do |plan|
  plan.use('deploy/default/install')

  plan.use('deploy/shared_logs')
  plan.use('deploy/nginx_symlink')

  # plan.use("deploy/dotenv_symlink")
  # or
  plan.use('rails/link_secret_key')

  plan.use('deploy/logrotator')
  plan.use('deploy/shared_folders', linked_folders: %w[tmp/cache tmp/sockets tmp/uploads public/uploads], folders: %w[env pids])

  plan.only_for_role :db do |sp|
    sp.use 'rails/db_migrate'
  end

  plan.use('deploy/init', action: 'install')
end

milestone :finalize_deploy do |plan|
  plan.use('deploy/default/finalize_deploy')

  plan.use('deploy/link_release')
  plan.use('deploy/init', action: 'start')
  plan.use('deploy/nginx_reload')
  plan.use('deploy/write_revision')
end
