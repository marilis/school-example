milestone :uninstall do |plan|
  plan.use("deploy/init", actions: %w{stop uninstall})
  plan.use("deploy/shared_logs/remove")
end
