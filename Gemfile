# frozen_string_literal: true

source 'https://gems.funbox.io' do
  gem 'explico', require: false
  gem 'unilog'
end

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.1'

gem 'execjs'
gem 'therubyracer'

gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.3'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'turbolinks', '~> 5'

# gem 'bootsnap', '>= 1.1.0', '< 1.4.2', require: false

group :development, :test do
  gem 'annotate'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 3.8'
end

group :development do
  gem 'binding_of_caller'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rubocop'
  gem 'rubocop-performance'
  gem 'web-console', '>= 3.3.0'
end

group :development, :test, :staging do
  gem 'factory_bot_rails'
  gem 'faker'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Project gems:
gem 'bulma-rails', '~> 0.7.4'
gem 'devise'
gem 'kaminari'
gem 'ransack'
gem 'simple_form'
gem 'slim'
