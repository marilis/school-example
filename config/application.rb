require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module SchoolsKarimov
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    # Don't generate system test files.

    config.secret_key_base = Rails.application.credentials[:secret_key_base]
    config.generators.system_tests = nil

    # enables Unilog's railtie
    config.unilog.enabled = true

    # enforces logging to stdout
    config.unilog.log_to_stdout = true

    # logging from rake-tasks into 'log/rake_tasks.log' file
    config.unilog.rake_log_to_stdout = false

    # an obvious option
    config.unilog.timestamp_precision = :microseconds

    # leave active record SQL trace as is
    config.unilog.remove_active_record_subscriptions = false

    # leave action view buzzing traces
    config.unilog.remove_action_view_subscriptions = false

    # do not touch action controller notifications
    config.unilog.remove_action_controller_subscriptions = false

    # message modification during logging
    config.unilog.modifiers << ->(message) { message.gsub(/password: [0-9]+/, "password: [FILTERED]") }
  end
end
