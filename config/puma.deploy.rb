require 'pathname'

APP_PATH  = Pathname.new('/srv/projects/schools-karimov')
CURRENT   = APP_PATH.join('current')
SHARED    = APP_PATH.join('shared')
PID       = SHARED.join('pids/puma.pid')
socket    = 'unix://' + SHARED.join('tmp/sockets/puma.sock').to_s

# See Puma::Configuration::DSL
bind        socket
pidfile     PID
threads     4, 16
state_path  SHARED.join('pids/puma.state')
workers     @options[:environment] == 'staging' ? 1 : 4
directory   CURRENT

rackup 'config.ru'
