# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  root 'index#index'
  resources :students, :teamleads, :teachers, :teams, :groups, :schools
end
